# 面试题：

1.如何临时退出一个正在交互的容器的终端， 而不终止它？	先ctrl+p，再ctrl+q，

2.docker的配置文件存放位置？ubuntu: /etc/default/docker; CentOS：/etc/sysconfig/docker

3.docker container run 创建并启动的时候使用-c 指定权重





# 笔记：

# 常用命令：

主机和宿主机复制文件：docker cp 主机路径 容器:/路径        docker cp 容器:/路径 主机路径

获取镜像或容器元数据： docker inspect mysql:5.6

docker log 容器

### 镜像命令

----------------------

##### 搜索镜像

```shell
$ docker search centos
```

##### 获取镜像

```shell
$ docker pull [选项] [Docker Registry 地址[:端口号]/]仓库名[:标签]
eg：$ docker pull centos:8
```

##### 列出镜像

```shell
$ docker image ls	#列出所有镜像
$ docker system df #命令来便捷的查看镜像、容器、数据卷所占用的空间。
```

##### 删除镜像

```shell
$ docker image rm [选项] <镜像1> [<镜像2> ...]
```


##### DockerFile文件：定制镜像

###### ` Dockerfile`文件

```dockerfile
FROM debian:stretch

RUN set -x; buildDeps='gcc libc6-dev make wget' \
    && apt-get update \
    && apt-get install -y $buildDeps \
    && wget -O redis.tar.gz "http://download.redis.io/releases/redis-5.0.3.tar.gz" \
    && mkdir -p /usr/src/redis \
    && tar -xzf redis.tar.gz -C /usr/src/redis --strip-components=1 \
    && make -C /usr/src/redis \
    && make -C /usr/src/redis install \
    && rm -rf /var/lib/apt/lists/* \
    && rm redis.tar.gz \
    && rm -r /usr/src/redis \
    && apt-get purge -y --auto-remove $buildDeps
    
    
#  DockerFile 命令详细：
#  https://vuepress.mirror.docker-practice.com/image/dockerfile/
```

###### 构建镜像

```shell
$ docker build -t nginx:v3 .
# 上一句中最后的一个 . 表示 镜像构建上下文
# 具体解释见：https://vuepress.mirror.docker-practice.com/image/build/#%E9%95%9C%E5%83%8F%E6%9E%84%E5%BB%BA%E4%B8%8A%E4%B8%8B%E6%96%87-context
```

###### RUN

*shell* 格式：`RUN <命令>`，类似直接在命令行中输入的命令一样，但每一个RUN就类似commit，RUN多层就会提交多层镜像。

*exec* 格式：`RUN ["可执行文件", "参数1", "参数2"]`，这更像是函数调用中的格式。

###### COPY

COPY [--chown=<user>:<group>] <源路径>... <目标路径>

`COPY` 指令将从构建上下文目录中 `<源路径>` 的文件/目录复制到新的一层的镜像内的 `<目标路径>` 位置。`<目标路径>` 可以是容器内的绝对路径，也可以是相对于工作目录的相对路径（工作目录可以用 `WORKDIR` 指令来指定）。目标路径不需要事先创建，如果目录不存在会在复制文件前先行创建缺失目录。

###### ENTRYPOINT

`ENTRYPOINT` 的格式和 `RUN` 指令格式一样，分为 `exec` 格式和 `shell` 格式。



这是因为当存在 `ENTRYPOINT` 后，`CMD` 的内容将会作为参数传给 `ENTRYPOINT`，而这里 `-i` 就是新的 `CMD`，因此会作为参数传给 `curl`，从而达到了我们预期的效果。

跟在镜像名后面的是 `command`，运行时会替换 `CMD` 的默认值







### 容器命令（容器的概念就是镜像的实例化）

##### 进入容器

```shell
$ docker exec -it webserver bash
```

##### 运行镜像(启动容器)

```shell
$ docker run -it --rm ubuntu:18.04 bash
# -it：这是两个参数，一个是 -i：交互式操作，一个是 -t 终端。我们这里打算进入 bash 执行一些命令并查看返回结果，因此我们需要交互式终端。
# --rm：这个参数是说容器退出后随之将其删除。默认情况下，为了排障需求，退出的容器并不会立即删除，除非手动 docker rm，使用 --rm 可以避免浪费空间。
# bash：放在镜像名后的是 命令，这里我们希望有个交互式 Shell，因此用的是 bash

cat /etc/os-release	#查看 Linux 常用的查看当前系统版本的命令
```

##### 后台运行

`-d`参数会返回唯一ID，输出结果不会在宿主机上输出，输出结果可以使用 ` docker logs ` 查看

##### 查看容器信息

```shell
$ docker container ls	# 获取容器信息  -a查看所有
$ docker container logs [container ID or NAMES]	#获取容器输出信息
```

##### 终止容器和重启容器

```shell
$ docker container stop #来终止一个运行中的容器。
#用户通过 exit 命令或 Ctrl+d 来退出终端时，所创建的容器立刻终止。
$ docker container ls -a  #查看终止状态的容器
$ docker container start  #将终止的容器重新启动
$ docker container restart #重启容器
```

##### 删除容器

```shell
$ docker container rm trusting_newton	#删除一个处于终止状态的容器
$ docker container prune	#可以清理掉所有处于终止状态的容器。
```



## Docker  -----------------

学习网址：

```
https://vuepress.mirror.docker-practice.com/
```

Docker镜像：

操作系统分为 **内核** 和 **用户空间**。对于 `Linux` 而言，内核启动后，会挂载 `root` 文件系统为其提供用户空间支持。而 **Docker 镜像**（`Image`），就相当于是一个 `root` 文件系统。比如官方镜像 `ubuntu:18.04` 就包含了完整的一套 Ubuntu 18.04 最小系统的 `root` 文件系统。

Docker容器：

Docker仓库：

#### 使用Docker镜像

#### 制作镜像

##### 镜像构建上下文：

现在就可以理解刚才的命令 `docker build -t nginx:v3 .` 中的这个 `.`，实际上是在指定上下文的目录，`docker build` 命令会将该目录下的内容打包交给 Docker 引擎以帮助构建镜像。

理解构建上下文对于镜像构建是很重要的，避免犯一些不应该的错误。比如有些初学者在发现 `COPY /opt/xxxx /app` 不工作后，于是干脆将 `Dockerfile` 放到了硬盘根目录去构建，结果发现 `docker build` 执行后，在发送一个几十 GB 的东西，极为缓慢而且很容易构建失败。那是因为这种做法是在让 `docker build` 打包整个硬盘，这显然是使用错误。

##### Dockerfile是一个用来构建镜像的文本文件，文本内容包含了一条条构建镜像所需的指令和说明。

#### 操作容器

新建并启动：`$ docker run ubuntu:18.04 /bin/echo 'Hello world'`

当利用 `docker run` 来创建容器时，Docker 在后台运行的标准操作包括：

- 检查本地是否存在指定的镜像，不存在就从 [registry](https://vuepress.mirror.docker-practice.com/repository/) 下载
- 利用镜像创建并启动一个容器
- 分配一个文件系统，并在只读的镜像层外面挂载一层可读写层
- 从宿主主机配置的网桥接口中桥接一个虚拟接口到容器中去
- 从地址池配置一个 ip 地址给容器
- 执行用户指定的应用程序
- 执行完毕后容器被终止

ps、top命令

后台运行：`$ docker run -d ubuntu:18.04 /bin/sh -c "while true; do echo hello world; sleep 1; done"`

终止：`docker container stop`

进入：`exec`命令

`docker exec` 后边可以跟多个参数，这里主要说明 `-i` `-t` 参数。

只用 `-i` 参数时，由于没有分配伪终端，界面没有我们熟悉的 Linux 命令提示符，但命令执行结果仍然可以返回。

当 `-i` `-t` 参数一起使用时，则可以看到我们熟悉的 Linux 命令提示符。

删除容器：docker container rm

#### Docker仓库

Docker Hub

#### 数据管理

#### 使用网络

使用 `hostPort:containerPort` 格式本地的 80 端口映射到容器的 80 端口，可以执行

```shell
$ docker run -d -p 80:80 nginx:alpine
```



#### 最佳实践

一个容器只运行一个进程：

应该保证在一个容器中只运行一个进程。将多个应用解耦到不同容器中，保证了容器的横向扩展和复用。例如 web 应用应该包含三个容器：web应用、数据库、缓存。

## Dcoker网络配置

```shell
$ docker logs 容器id
# 查看容器访问记录
$ docker port 容器id
# 查看端口映射配置
```

容器互联

```shell
# 创建一个网络
$ docker network create -d bridge 名称
# 容器连接到创建的网络上 --network 名称
$ docker run -it --rm --name busybox1 --network 名称 busybox sh
```

## Docker-compose:定义和运行多个 Docker 容器的应用

它允许用户通过一个单独的 `docker-compose.yml` 模板文件（YAML 格式）来定义一组相关联的应用容器为一个项目（project）。

- 服务 (`service`)：一个应用的容器，实际上可以包括若干运行相同镜像的容器实例。
- 项目 (`project`)：由一组关联的应用容器组成的一个完整业务单元。

#### compose命令：

##### up

它将尝试自动完成包括构建镜像，（重新）创建服务，启动服务，并关联服务相关容器的一系列操作。默认情况，如果服务容器已经存在，`docker-compose up` 将会尝试停止容器，然后重新创建（保持使用 `volumes-from` 挂载的卷），以保证新启动的服务匹配 `docker-compose.yml` 文件的最新内容。

docker-compose up -d 后台运行，推荐使用

#### Compose模板文件：

***每个服务都必须通过 `image` 指令指定镜像或 `build` 指令（需要 Dockerfile）等来自动构建生成镜像。***

官网文档：https://docs.docker.com/compose/compose-file/compose-file-v3/

##### build

指定 `Dockerfile` 所在文件夹的路径（可以是绝对路径，或者相对 docker-compose.yml 文件的路径）。 `Compose` 将会利用它自动构建这个镜像，然后使用这个镜像。

##### container_name

指定容器名称 container_name: docker-web-container

##### devices                                                                                                                                                                            

指定设备映射关系 

```yaml
devices:
  - "/dev/ttyUSB1:/dev/ttyUSB0"
```

##### environment

设置环境变量。你可以使用数组或字典两种格式。只给定名称的变量会自动获取运行 Compose 主机上对应变量的值，可以用来防止泄露不必要的数据。

```yaml
environment:
  - RACK_ENV=development
  - SESSION_SECRET
```

##### expose

暴露端口，但不映射到宿主机，只被连接的服务访问。仅可以指定内部端口为参数。

##### external_links

链接到 `docker-compose.yml` 外部的容器，甚至并非 `Compose` 管理的外部容器。

```yaml
external_links:
 - redis_1
 - project_db_1:mysql
 - project_db_1:postgresql
```

##### extra_hosts

类似 Docker 中的 `--add-host` 参数，指定额外的 host 名称映射信息。***会在启动后的服务容器中 `/etc/hosts` 文件中添加如下两条条目。***

```yaml
extra_hosts:
 - "googledns:8.8.8.8"
 - "dockerhub:52.1.157.61"
```

##### network_mode

设置网络模式。使用和 `docker run` 的 `--network` 参数一样的值。

```yaml
network_mode: "bridge"
network_mode: "host"
network_mode: "none"
network_mode: "service:[service name]"
network_mode: "container:[container name/id]"
```

##### networks

配置容器连接的网络。

```yaml
version: "3"
services:

  some-service:
    networks:
     - some-network
     - other-network

networks:
  some-network:
  other-network:
```

##### ports

暴露端口信息。使用宿主端口：容器端口 `(HOST:CONTAINER)` 格式，或者仅仅指定容器的端口（宿主将会随机选择端口）都可以。

```yaml
ports:
 - "3000"
 - "8000:8000"
 - "49100:22"
 - "127.0.0.1:8001:8001"
```

*注意：当使用 `HOST:CONTAINER` 格式来映射端口时，如果你使用的容器端口小于 60 并且没放到引号里，可能会得到错误结果，因为 `YAML` 会自动解析 `xx:yy` 这种数字格式为 60 进制。为避免出现这种问题，建议数字串都采用引号包括起来的字符串格式。*

##### volumes

数据卷所挂载路径设置。可以设置为宿主机路径(`HOST:CONTAINER`)或者数据卷名称(`VOLUME:CONTAINER`)，并且可以设置访问模式 （`HOST:CONTAINER:ro`）。该指令中路径支持相对路径。

```yaml
volumes:
 - /var/lib/mysql
 - cache/:/tmp/cache
 - ~/configs:/etc/configs/:ro
```











#### 持续集成，持续部署

**持续集成(Continuous integration)** 是一种软件开发实践，每次集成都通过自动化的构建（包括编译，发布，自动化测试）来验证，从而尽早地发现集成错误。

**持续部署(continuous deployment)** 是通过自动化的构建、测试和部署循环来快速交付高质量的产品。

与 `Jenkins` 不同的是，基于 Docker 的 CI/CD 每一步都运行在 Docker 容器中，所以理论上支持所有的编程语言。
