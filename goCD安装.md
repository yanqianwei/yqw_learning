### 安装goCD Server

获取镜像：`docker pull gocd/gocd-server:v21.3.0`

运行镜像：`run docker run -d -p8153:8153 gocd/gocd-server:v21.3.0`

启动成功后可以访问仪表盘：`localhost:8153`

### 安装goCD Agent

获取镜像：`docker pull gocd/gocd-agent-centos-7:v21.3.0`

运行镜像：`docker run -d -e GO_SERVER_URL=... gocd/gocd-agent-centos-7:v21.3.0`

 The `GO_SERVER_URL` must be an HTTPS url and end with `/go`, 

for e.g. ` http://ip.add.re.ss:8153/go ` 









注意：不管是server还是agent都需要指定版本，没有所谓的laster版本





