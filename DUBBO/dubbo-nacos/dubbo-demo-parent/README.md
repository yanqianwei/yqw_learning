### yqw_learning

#### 1.先建立了Maven的父子工程

父工程pom文件中添加如下依赖：

```xml
<properties>
        <java.version>1.8</java.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <!--Spring-->
        <spring.boot.version>2.2.13.RELEASE</spring.boot.version>
        <spring.cloud.alibaba.version>2.2.6.RELEASE</spring.cloud.alibaba.version>
</properties>

    <!--Spring版本-->
    <dependencyManagement>
        <dependencies>
<!--     在子项目中指定了nacos-client的版本，可以不要了，便于理解！！！
      <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${spring.cloud.alibaba.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring.boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <!-- 子工程大多数的依赖来自于此 -->
                <groupId>org.apache.dubbo</groupId>
                <artifactId>dubbo-bom</artifactId>
                <version>3.0.2.1</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <!-- 打包插件，再次记录温习一下，大概意思是：可以将项目依赖的jar包打包到可运行的jar文件中。 -->
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <id>aliyun-repos</id>
            <url>https://maven.aliyun.com/nexus/content/groups/public/</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>apache.snapshots.https</id>
            <name>Apache Development Snapshot Repository</name>
            <url>https://repository.apache.org/content/repositories/snapshots</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <id>aliyun-plugin</id>
            <url>https://maven.aliyun.com/nexus/content/groups/public/</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>
```

#### 2.建立interface api工程，创建声明接口和方法即可，无需多余操作，pom文件默认，在后面的Moudel中，需要将现在的工程引入。

#### 3.建立provider工程（提供服务）

pom文件内容：

```xml
<properties>
        <java.version>1.8</java.version>
    <!-- 感觉下面声明的这个版本也没有使用，可以删掉，但没有尝试 -->
        <dubbo.version>3.0.2</dubbo.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo-registry-nacos</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.nacos</groupId>
            <artifactId>nacos-client</artifactId>
       <!-- 此处执行了 nacos-client 的版本，就不需要从父工程继承，也就是之前说的父工程的一个dependency就不要了 -->
            <version>2.0.3</version>
        </dependency>
        <dependency>
            <!-- 前一步创建的api -->
            <groupId>org.example</groupId>
            <artifactId>dubbo-demo-produce-api</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.apache.dubbo/dubbo-spring-boot-starter -->
        <dependency>
            <!-- 这玩意自继承 dubbo-bom -->
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo-spring-boot-starter</artifactId>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
```

配置文件application.yml

```yml
spring:
 application:
  name: dubbo-demo-produce
server:
#当前的启动端口
 port: 8888
dubbo:
  registry:
#本地的nacos的地址
   address: nacos://127.0.0.1:8848
#声明的是Dubbo的协议和端口，也就是RPC协议，服务与服务通信的协议。
   protocol:
    port: 20880
    name: dubbo
```

实现api的接口：

```java
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.rpc.RpcContext;
import org.example.DemoService;
import org.springframework.stereotype.Service;

//Dubbo的注解声明一个服务
@DubboService(version = "1.0.0")
@Service
public class DemoServiceImpl implements DemoService {
    public String sayName(String name) {
        return name + "-------------------------";
    }

    public String sayHello(String name) {
        return "Hello " + name + ", response from provide yqw: " + RpcContext.getServiceContext().getLocalAddress();
    }

}
```

启动类：

```java
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//注解扫描，也就是前面的实现了api的service impl 所在的package。
@EnableDubbo(scanBasePackages = "org.example.service")
public class DubboDemoProduceApplication {
    public static void main(String[] args) {
        SpringApplication.run(DubboDemoProduceApplication.class, args);
    }
}

```

#### 4.建立Consume

启动类类似，pom文件类似，配置文件如下：（改变的只有name和端口）

```yml
spring:
 application:
  name: dubbo-demo-consume
server:
 port: 8889
dubbo:
 application:
  name: ${spring.application.name}
 registry:
  address: nacos://127.0.0.1:8848
  protocol:
   port: 20880
   name: dubbo
```

引用Dubbo的api接口，使用注解： @DubboReference(check = false,version = "1.0.0")。

check属性：启动时检查提供者是否存在，true报错，false忽略

详情：https://dubbo.apache.org/zh/docs/v2.7/user/references/xml/dubbo-reference/

