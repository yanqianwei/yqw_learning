package org.example;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo(scanBasePackages = "org.example.controller")
public class DubboDemoConsumeApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboDemoConsumeApplication.class, args);
    }

}
