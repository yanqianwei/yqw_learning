package org.example.service;

import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.rpc.RpcContext;
import org.example.DemoService;
import org.springframework.stereotype.Service;

//Dubbo的注解声明一个服务
@DubboService(version = "1.0.0")
@Service
public class DemoServiceImpl implements DemoService {
    public String sayName(String name) {
        return name + "-------------------------";
    }

    public String sayHello(String name) {
        return "Hello " + name + ", response from provider: " + RpcContext.getServiceContext().getLocalAddress();
    }

}
