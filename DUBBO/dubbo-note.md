## Dubbo

#### 服务发现

我们提倡直接启用 Dubbo3 的默认行为，即启用应用级服务发现。

#### RPC 通信协议

Dubbo3 提供了 Triple(Dubbo3)、Dubbo2 协议，这是 Dubbo 框架的原生协议。

协议的内容包含三部分：

- 数据交换格式： 定义 RPC 的请求和响应对象在网络传输中的字节流内容，也叫作序列化方式
- 协议结构： 定义包含字段列表和各字段语义以及不同字段的排列方式
- 协议通过定义规则、格式和语义来约定数据如何在网络间传输

Triple Streaming

#### 服务流量管理

- [VirtualService](http://dubbo.apache.org/zh/docs/references/routers/virtualservice/)主要处理入站流量分流的规则，支持服务级别和方法级别的分流。
- [DubboRoute](http://dubbo.apache.org/zh/docs/references/routers/virtualservice/#dubboroute)主要解决服务级别的分流问题。同时，还提供的重试机制、超时、故障注入、镜像流量等能力。
- [DubboRouteDetail](http://dubbo.apache.org/zh/docs/references/routers/virtualservice/#dubboroutedetail)主要解决某个服务中方法级别的分流问题。支持方法名、方法参数、参数个数、参数类型、header等各种维度的分流能力。同时也支持方法级的重试机制、超时、故障注入、镜像流量等能力。
- [DubboDestination](http://dubbo.apache.org/zh/docs/references/routers/virtualservice/#dubbodestination)用来描述路由流量的目标地址，支持host、port、subnet等方式。
- [DestinationRule](http://dubbo.apache.org/zh/docs/references/routers/destination-rule/)主要处理目标地址规则，可以通过hosts、[subnet](http://dubbo.apache.org/zh/docs/references/routers/destination-rule/#subset)等方式关联到Provider集群。同时可以通过[trafficPolicy](http://dubbo.apache.org/zh/docs/references/routers/destination-rule/#trafficpolicy)来实现负载均衡。

#### 部署架构（注册中心 配置中心 元数据中心）