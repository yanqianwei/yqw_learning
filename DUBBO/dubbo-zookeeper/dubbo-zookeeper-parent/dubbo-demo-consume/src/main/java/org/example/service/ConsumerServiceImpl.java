package org.example.service;

import org.apache.dubbo.config.annotation.DubboService;
import org.example.DemoServiceForCons;
import org.springframework.stereotype.Service;

@DubboService(version = "2.0.0")
@Service
public class ConsumerServiceImpl implements DemoServiceForCons {

    public String hiDad() {
        return "hi , yqw dad========";
    }
}
