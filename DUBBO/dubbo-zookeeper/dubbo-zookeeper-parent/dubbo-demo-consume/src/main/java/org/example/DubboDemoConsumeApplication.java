package org.example;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//配置了启动时扫描的注解包，使用了Dubbo的注解的。
@EnableDubbo(scanBasePackages = "org.example.*")
public class DubboDemoConsumeApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboDemoConsumeApplication.class, args);
    }

}
