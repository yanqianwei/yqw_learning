package org.example.controller;

import org.apache.dubbo.config.annotation.DubboReference;
import org.example.DemoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class TestController {

    @DubboReference(check = false,version = "1.0.0")
    DemoService demoService;

    @RequestMapping("/test")
    public String test(){

        return demoService.sayName("yqw is your dad!!!!==============");
    }

    @RequestMapping("/test2")
    public String test2(){

        return demoService.sayHello("yqw is your dad!!!!==============");
    }
}
