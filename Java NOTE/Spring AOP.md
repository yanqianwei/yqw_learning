### Spring IOC

控制反转，应用本身不负责依赖对象的创建和维护，依赖对象的创建和维护是由外部容器负责的称为控制反转。

### Spring AOP

应用场景：

 日志记录、异常处理、权限验证、缓存处理、事务处理、数据持久化、效率检查、内容分发

aspect：切面

pointcut：切点

joinpoint：连接点

weaving：织入

advice：通知

target：目标对象

aop Proxy：代理对象

### @Autowired

首先在容器中查找对应**类型**的bean，如果查询结果为一个，直接装配，如果查询结果是多个，会根据名称查找。









